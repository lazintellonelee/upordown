var express         = require('express');
var path            = require('path');
var favicon         = require('serve-favicon');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var i18n            = require('i18n');

i18n.configure({
    locales         : ['en', 'vi'],
    defaultLocale   : 'vi',
    cookie          : 'locale',
    directory       : __dirname + '/locales'
});

var routes  = require('./routes/index');

var app     = express();
var server  = app.listen(3000, function(){
    console.log('Listenning port 3000');
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(i18n.init);
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/themes', express.static(path.join(__dirname, 'public/themes')));
app.use('/plugins', express.static(path.join(__dirname, 'node_modules')));
app.use(function(req, res, next) {
    if(!req.cookies.locale)
    {
        req.setLocale('vi');
        res.cookie('locale', 'vi', {
            expires : new Date(Date.now() + 2592000)
        });
    }
    next();
});

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
